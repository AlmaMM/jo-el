import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Scanner;

public class abba {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int loops = sc.nextInt();
        sc.nextLine();
        for (int i = 0; i < loops; i++) {
            String members = sc.nextLine();
            String membersNorm = Normalizer.normalize(members, Form.NFD);
            String membersFinal = membersNorm.replaceAll("\\p{M}", "");
            String[] split = membersFinal.split(", ");
            String[] lastTwoMembers = split[split.length - 1].split(" i ");
            String[] membersDefinitive = new String[split.length + 1];
            for (int j = 0; j < split.length; j++) {
                membersDefinitive[j] = split[j];
            }
            membersDefinitive[membersDefinitive.length - 2] = lastTwoMembers[0];
            membersDefinitive[membersDefinitive.length - 1] = lastTwoMembers[1];

            String word = "";
            for (String member : membersDefinitive) {
                member = member.toUpperCase();
                word += member.charAt(0);
            }
            System.out.println(word);
        }
        sc.close();
    }

}
